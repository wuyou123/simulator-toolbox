function eul = quatToEuler_PX4( q )
% INPUT q [Nx4] matrix containing N quaternions in PX4 notation [w x y x]
% OUTPUT eul [Nx3] matrix containing the corresponding euler angles RPY
% 
% Requires function quatToEuler from the simulator-toolbox repository.
% https://gitlab.com/flyart/simulator-toolbox.git

% Authors
% Simone Panza <simone.panza@polimi.it>
% Politecnico di Milano 
% Date 2018_06_19
% changelog:
%   v1
%       first version

N = size(q,1);

%%% reorder quaternion as from our notation [x y z w]
qq = zeros(size(q));
qq(:,1:3) = q(:,2:4);
qq(:,4) = q(:,1);

%%% compute euler angles
eul = zeros(N,3);
for i = 1:N
    eul(i,:) = quatToEuler(qq(i,:)');
end

end

