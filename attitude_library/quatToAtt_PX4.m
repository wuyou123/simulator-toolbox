function A = quatToAtt_PX4( q )
% INPUT q [Nx4] matrix containing N quaternions in PX4 notation [w x y x]
% OUTPUT A [Nx3x3] matrix containing the corresponding attitude matrix
% 
% Requires function quatToAtt from the simulator-toolbox repository.
% https://gitlab.com/flyart/simulator-toolbox.git

% Authors
% Mattia Giurato <mattia.giurato@polimi.it>
% Politecnico di Milano 
% Date 2018_08_26
% changelog:
%   v1
%       first version

N = size(q,1);

%%% reorder quaternion as from our notation [x y z w]
qq = zeros(size(q));
qq(:,1:3) = q(:,2:4);
qq(:,4) = q(:,1);

%%% compute attitude matrix
A = zeros(3,3,N);
for i = 1:N
    A(:,:,i) = quatToAtt(qq(i,:));
end

end